Project Base Login

1. Initialize Yarn on main folder: yarn init -y
2. Add Express: yarn add express
3. Add Sucrase and Nodemon and ESLint with Development Scope: yarn add sucrase nodemon -D
4. Add ESLint and Prettier with Development Scope: yarn add eslint prettier eslint-config-prettier eslint-plugin-prettier -D
5. Add Sequelize: yarn add sequelize
6. Add Sequelize-CLI with Development Scope: yarn add sequelize-cli -D
7. Add PG and PG-HStore: yarn add pg pg-hstore
8. Add BCrypt: yarn add bcryptjs
9. Add JWT: yarn add jsonwebtoken
